/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package jp.co.teraintl.g12011.wforecasterd.wforecast.cnvi;

public final class R {
    public static final class attr {
    }
    public static final class drawable {
        public static final int background_widget=0x7f020000;
        public static final int ic_launcher=0x7f020001;
        public static final int icon=0x7f020002;
        public static final int wh_cld_64px=0x7f020003;
        public static final int wh_cld_occa_ran_64px=0x7f020004;
        public static final int wh_cld_occa_snw_64px=0x7f020005;
        public static final int wh_cld_occa_sun_64px=0x7f020006;
        public static final int wh_cld_occa_ths_64px=0x7f020007;
        public static final int wh_cld_then_ran_64px=0x7f020008;
        public static final int wh_cld_then_snw_64px=0x7f020009;
        public static final int wh_cld_then_sun_64px=0x7f02000a;
        public static final int wh_cld_then_ths_64px=0x7f02000b;
        public static final int wh_ran_64px=0x7f02000c;
        public static final int wh_ran_occa_cld_64px=0x7f02000d;
        public static final int wh_ran_occa_snw_64px=0x7f02000e;
        public static final int wh_ran_occa_sun_64px=0x7f02000f;
        public static final int wh_ran_occa_ths_64px=0x7f020010;
        public static final int wh_ran_then_cld_64px=0x7f020011;
        public static final int wh_ran_then_snw_64px=0x7f020012;
        public static final int wh_ran_then_sun_64px=0x7f020013;
        public static final int wh_ran_then_ths_64px=0x7f020014;
        public static final int wh_snw_64px=0x7f020015;
        public static final int wh_snw_occa_cld_64px=0x7f020016;
        public static final int wh_snw_occa_ran_64px=0x7f020017;
        public static final int wh_snw_occa_sun_64px=0x7f020018;
        public static final int wh_snw_occa_ths_64px=0x7f020019;
        public static final int wh_snw_then_cld_64px=0x7f02001a;
        public static final int wh_snw_then_ran_64px=0x7f02001b;
        public static final int wh_snw_then_sun_64px=0x7f02001c;
        public static final int wh_snw_then_ths_64px=0x7f02001d;
        public static final int wh_sun_64px=0x7f02001e;
        public static final int wh_sun_occa_cld_64px=0x7f02001f;
        public static final int wh_sun_occa_ran_64px=0x7f020020;
        public static final int wh_sun_occa_snw_64px=0x7f020021;
        public static final int wh_sun_occa_ths_64px=0x7f020022;
        public static final int wh_sun_then_cld_64px=0x7f020023;
        public static final int wh_sun_then_ran_64px=0x7f020024;
        public static final int wh_sun_then_snw_64px=0x7f020025;
        public static final int wh_sun_then_ths_64px=0x7f020026;
        public static final int wh_ths_64px=0x7f020027;
        public static final int wh_ths_occa_cld_64px=0x7f020028;
        public static final int wh_ths_occa_ran_64px=0x7f020029;
        public static final int wh_ths_occa_snw_64px=0x7f02002a;
        public static final int wh_ths_occa_sun_64px=0x7f02002b;
        public static final int wh_ths_then_cld_64px=0x7f02002c;
        public static final int wh_ths_then_ran_64px=0x7f02002d;
        public static final int wh_ths_then_snw_64px=0x7f02002e;
        public static final int wh_ths_then_sun_64px=0x7f02002f;
    }
    public static final class id {
        public static final int AdministrativeAreaComment=0x7f060016;
        public static final int AdministrativeAreaCommentLabel=0x7f060014;
        public static final int AdministrativeAreaCommentPubDate=0x7f060015;
        public static final int AuthorsCredit=0x7f06001d;
        public static final int AuthorsCreditSubject=0x7f06001c;
        public static final int ChanceOfRainLabel=0x7f06000c;
        public static final int ChoseAreaActivityComment=0x7f060018;
        public static final int ImageTodaysWeather=0x7f060024;
        public static final int InterimChosenAreaName=0x7f060017;
        public static final int NamesOfPlaces=0x7f060019;
        public static final int RainFallProvFourQuarters=0x7f060010;
        public static final int RainFallProvOneQuarters=0x7f06000d;
        public static final int RainFallProvThreeQuarters=0x7f06000f;
        public static final int RainFallProvTwoQuarters=0x7f06000e;
        public static final int Separator=0x7f060002;
        public static final int StringDate=0x7f060004;
        public static final int StringDayOfWeek=0x7f060005;
        public static final int StringWeather=0x7f060006;
        public static final int TempMax=0x7f060008;
        public static final int TempMaxLabel=0x7f060007;
        public static final int TempMin=0x7f06000a;
        public static final int TempMinLabel=0x7f060009;
        public static final int TempMinSuffix=0x7f06000b;
        public static final int TextAdministrativeAreaName=0x7f06001f;
        public static final int TextLocalityName=0x7f060020;
        public static final int TextTodaysDate=0x7f060021;
        public static final int TextTodaysHighestTemperature=0x7f060022;
        public static final int TextTodaysLowestTemperature=0x7f060023;
        public static final int WeatherIcon=0x7f060003;
        public static final int WholeOfCountryComment=0x7f060013;
        public static final int WholeOfCountryCommentLabel=0x7f060011;
        public static final int WholeOfCountryCommentPubDate=0x7f060012;
        public static final int WindDir=0x7f06001a;
        public static final int WindSpeed=0x7f06001b;
        public static final int monitoring_point_label=0x7f060000;
        public static final int widget_base=0x7f06001e;
        public static final int widget_id_and_location_type=0x7f060001;
    }
    public static final class layout {
        public static final int frcst_activity=0x7f030000;
        public static final int frcst_activity_row=0x7f030001;
        public static final int frcst_activity_row_footer=0x7f030002;
        public static final int frcst_chose_area=0x7f030003;
        public static final int frcst_chose_area_row=0x7f030004;
        public static final int frcst_wwonline_activity_row=0x7f030005;
        public static final int frcst_wwonline_activity_row_footer=0x7f030006;
        public static final int frcstmtrlg_widget=0x7f030007;
    }
    public static final class string {
        public static final int activity_label_chose_area=0x7f050048;
        public static final int activity_label_foreign_chose_area=0x7f050049;
        /**  ウィジェットフリック時の詳細画面関係 
         */
        public static final int activity_label_weather_activity_label=0x7f050047;
        /**  アプリケーション名 
         */
        public static final int app_name=0x7f050000;
        public static final int assets_csv_m_country_default=0x7f050041;
        public static final int assets_csv_m_p_administrative_area_default=0x7f050043;
        public static final int assets_csv_m_p_locality_default=0x7f050045;
        public static final int assets_csv_m_p_region_default=0x7f050042;
        public static final int assets_csv_m_p_sub_administrative_area_default=0x7f050044;
        /**  テーブル初期値設定CSVファイル一覧 
         */
        public static final int assets_csv_t_widget_locale_history=0x7f050040;
        /**  WorldWeatherOnline の天気予報定義ファイル 
         */
        public static final int assets_data_wwonline_weather_code_ja=0x7f050046;
        public static final int assets_sql_common=0x7f05003c;
        public static final int assets_sql_m_country=0x7f050035;
        public static final int assets_sql_m_p_administrative_area=0x7f050037;
        public static final int assets_sql_m_p_foreign_city=0x7f05003a;
        public static final int assets_sql_m_p_locality=0x7f050039;
        public static final int assets_sql_m_p_region=0x7f050036;
        public static final int assets_sql_m_p_sub_administrative_area=0x7f050038;
        public static final int assets_sql_monitoring_point=0x7f05003b;
        public static final int assets_sql_t_adar_weather_comment=0x7f05003f;
        public static final int assets_sql_t_weather_history=0x7f05003d;
        /**  各SQLファイルの定義 
         */
        public static final int assets_sql_t_widget_locale_history=0x7f050034;
        public static final int assets_sql_t_woc_weather_comment=0x7f05003e;
        public static final int authors_wwonline_credit=0x7f050010;
        /**  WorldWeatherOnline クレジット表示関連 
         */
        public static final int authors_wwonline_credit_subject=0x7f05000f;
        public static final int database_name=0x7f050029;
        /**  DBのバージョン 
         */
        public static final int database_version=0x7f050028;
        /**  地域情報デフォルト値 
         */
        public static final int gps_default_latitude=0x7f050002;
        public static final int gps_default_longitude=0x7f050003;
        public static final int http_request_interval=0x7f050012;
        public static final int http_request_num=0x7f050013;
        public static final int intent_extra_administrative_area_name=0x7f05004d;
        public static final int intent_extra_country_name=0x7f05004c;
        public static final int intent_extra_country_name_code=0x7f05004b;
        public static final int intent_extra_load_last_ref_location_flg=0x7f05004f;
        public static final int intent_extra_locality_name=0x7f05004e;
        /**  インテント関連 
         */
        public static final int intent_extra_widget_id=0x7f05004a;
        public static final int interspace_date=0x7f050051;
        /**  新生ウィジェット寿命、新生/恒常更新ウィジェットの更新間隔の設定【分単位指定】 
         */
        public static final int interval_baby_widget=0x7f05000d;
        /**  恒常的にウィジェットを更新する間隔 
         */
        public static final int interval_constant_widget=0x7f05000e;
        /**  DBクリーンアップ関連の設定値 
         */
        public static final int interval_db_cleanup=0x7f050050;
        /**  新生ウィジェット寿命(上限更新回数) 
         */
        public static final int life_span_of_baby_widget=0x7f05000c;
        public static final int location_default_accuracy=0x7f050005;
        public static final int location_default_administrative_area_name=0x7f050008;
        public static final int location_default_country_name=0x7f050007;
        public static final int location_default_country_name_code=0x7f050006;
        public static final int location_default_gps_flag=0x7f05000b;
        public static final int location_default_json_id=0x7f050004;
        public static final int location_default_locality_name=0x7f05000a;
        public static final int location_default_sub_administrative_area_name=0x7f050009;
        public static final int m_country=0x7f05002b;
        public static final int m_p_administrative_area=0x7f05002d;
        public static final int m_p_foreign_city=0x7f050030;
        public static final int m_p_locality=0x7f05002f;
        public static final int m_p_region=0x7f05002c;
        public static final int m_p_sub_administrative_area=0x7f05002e;
        /**  パッケージ名 
         */
        public static final int name_package_cnvi=0x7f050052;
        /**  気象予報RSSのネームスペース接頭辞 
         */
        public static final int rss_ns_uri_pfx_wm=0x7f050027;
        public static final int t_adar_weather_comment=0x7f050032;
        public static final int t_weather_history=0x7f050031;
        /**  テーブル名一覧 
         */
        public static final int t_widget_locale_history=0x7f05002a;
        public static final int t_woc_weather_comment=0x7f050033;
        /**  Http クライアント関連の設定値(World weather online) 
         */
        public static final int url_authorigy_free_wwo=0x7f05001e;
        public static final int url_free_wwo_path=0x7f05001f;
        public static final int url_free_wwo_query_param_key_api_key=0x7f050025;
        public static final int url_free_wwo_query_param_key_format=0x7f050021;
        public static final int url_free_wwo_query_param_key_latlon=0x7f050020;
        public static final int url_free_wwo_query_param_key_num_of_days=0x7f050023;
        public static final int url_free_wwo_query_param_value_api_key=0x7f050026;
        public static final int url_free_wwo_query_param_value_format_json=0x7f050022;
        public static final int url_free_wwo_query_param_value_num_of_days_5=0x7f050024;
        /**  Http クライアント関連の設定値(Goole maps API) 
         */
        public static final int url_geo_authority=0x7f050014;
        public static final int url_geo_path=0x7f050015;
        public static final int url_geo_queryParamKey_apikey=0x7f050019;
        public static final int url_geo_queryParamKey_hl=0x7f05001a;
        public static final int url_geo_queryParamKey_ll=0x7f050016;
        public static final int url_geo_queryParamKey_output=0x7f050017;
        public static final int url_geo_queryParamValue_hl=0x7f05001b;
        public static final int url_geo_queryParamValue_oe=0x7f05001d;
        public static final int url_geo_queryParamValue_output=0x7f050018;
        public static final int url_geo_queryparamkey_oe=0x7f05001c;
        /**  Http クライアント関連の設定値(Goole maps API) 
         */
        public static final int url_scheme_http=0x7f050011;
        /** 
		気象情報を提供するプロバイダを定義する
		1:ウェザーマップ(一口天気予報RSS)
		2:WorldWeatherOnline
	
         */
        public static final int weather_provider_default=0x7f050001;
    }
    public static final class xml {
        public static final int appwidget=0x7f040000;
    }
}
